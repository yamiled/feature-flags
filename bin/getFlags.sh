#!/bin/sh

GITLAB_REPO=https://gitlab.com/gitlab-org/gitlab.git
FLAGS_DIR=config/feature_flags/

mkdir -p data/flags
cd data && rm -f flags/*.yml && rm -rf gitlab

git clone \
  --depth 1 \
  --filter=blob:none \
  --sparse \
  $GITLAB_REPO \
;

cd gitlab
git sparse-checkout set $FLAGS_DIR

cp ${FLAGS_DIR}/*/*.yml ../flags

FLAGS_DIR=ee/config/feature_flags/

git sparse-checkout set $FLAGS_DIR

cp ${FLAGS_DIR}/*/*.yml ../flags
