const fs = require("fs");
const yaml = require("js-yaml");

const FLAGS_DIR = "data/flags/";
const OUTPUT = "data/flags.json";

const getFlags = new Promise((res, rej) => {
  return fs.readdir(FLAGS_DIR, (err, filenames) => {
    if (err) {
      rej(err);
    }
    let flags = [];
    filenames.forEach((filename) => {
      const flag = yaml.load(
        fs.readFileSync(`${FLAGS_DIR}/${filename}`, "utf-8")
      );

      if (!flag.milestone) {
        flag.milestone = "unknown";
      }

      // Because sometimes "13.0" is parsed as 13
      if (typeof flag.milestone !== "string") {
        flag.milestone = `${flag.milestone}.0`;
      }

      flags.push(flag);
    });
    res(flags);
  });
});

getFlags.then((flags) => {
  fs.writeFileSync(OUTPUT, JSON.stringify(flags, null, 2));
});
