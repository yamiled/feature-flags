const defaultEnabled = (flags, filter) =>
  typeof filter === "boolean"
    ? flags.filter(({ default_enabled }) => default_enabled === filter)
    : flags;

export default defaultEnabled;
