const search = (flags, filter) =>
  filter ? flags.filter(({ name }) => name.match(filter)) : flags;

export default search;
