const typeFilter = (flags, types = []) =>
  types[0]?.data
    ? types.reduce(
        (_flags, filter) => [
          ..._flags,
          ...flags.filter(({ type }) => type === filter.data),
        ],
        []
      )
    : flags;

export default typeFilter;
