import compareVersionNumbers from "../utils/compareVersionNumbers";

const findByOperator = (tokens, op) =>
  tokens.find(({ operator }) => operator === op)?.data;

const milestoneFilter = (flags, milestones = []) =>
  milestones[0]?.data
    ? (() => {
        let filteredFlags = flags;
        const eq = findByOperator(milestones, "=");
        const gt = findByOperator(milestones, ">");
        const lt = findByOperator(milestones, "<");

        if (eq) {
          filteredFlags = filteredFlags.filter(
            ({ milestone }) => milestone === eq
          );
        } else {
          if (gt) {
            filteredFlags = filteredFlags.filter(
              ({ milestone }) =>
                milestone && compareVersionNumbers(milestone, gt, ">")
            );
          }
          if (lt) {
            filteredFlags = filteredFlags.filter(
              ({ milestone }) =>
                milestone && compareVersionNumbers(milestone, lt, "<")
            );
          }
        }
        return filteredFlags;
      })()
    : flags;

export default milestoneFilter;
