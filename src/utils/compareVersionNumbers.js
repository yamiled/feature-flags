const compareVersionNumbers = (a, b, operator) => {
  if (isNaN(a)) {
    return true;
  }
  if (isNaN(b)) {
    return false;
  }
  const [aMain, aMinor] = a.split(".").map((int) => parseInt(int));
  const [bMain, bMinor] = b.split(".").map((int) => parseInt(int));

  switch (operator) {
    case ">":
      if (aMain === bMain) {
        return aMinor > bMinor;
      }
      return aMain > bMain;

    case "<":
      if (aMain === bMain) {
        return aMinor < bMinor;
      }
      return aMain < bMain;
    default:
      return a === b;
  }
};

export default compareVersionNumbers;
